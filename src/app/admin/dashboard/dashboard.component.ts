import {Component, OnInit} from '@angular/core';
import * as Chartist from 'chartist';
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import {Game} from '../../shared/models/Game';
import {Team} from '../../shared/models/Team';

declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})


export class DashboardComponent implements OnInit {

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };
  private AddBarUrl = 'http://192.168.1.33:5000/bar/';
  private AddGameUrl = 'assets/api/gamesList/gamesList.json';

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    var dataSales = {
      labels: ['9:00AM', '12:00AM', '3:00PM', '6:00PM', '9:00PM', '12:00PM', '3:00AM', '6:00AM'],
      series: [
        [287, 385, 490, 562, 594, 626, 698, 895, 952],
        [67, 152, 193, 240, 387, 435, 535, 642, 744],
        [23, 113, 67, 108, 190, 239, 307, 410, 410]
      ]
    };

    var optionsSales = {
      low: 0,
      high: 1000,
      showArea: true,
      height: '245px',
      axisX: {
        showGrid: false,
      },
      lineSmooth: Chartist.Interpolation.simple({
        divisor: 3
      }),
      showLine: true,
      showPoint: false,
    };

    var responsiveSales: any[] = [
      ['screen and (max-width: 640px)', {
        axisX: {
          labelInterpolationFnc: function (value) {
            return value[0];
          }
        }
      }]
    ];

    new Chartist.Line('#chartHours', dataSales, optionsSales, responsiveSales);


    var data = {
      labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      series: [
        [542, 543, 520, 680, 653, 753, 326, 434, 568, 610, 756, 895],
        [230, 293, 380, 480, 503, 553, 600, 664, 698, 710, 736, 795]
      ]
    };

    var options = {
      seriesBarDistance: 10,
      axisX: {
        showGrid: false
      },
      height: '245px'
    };

    var responsiveOptions: any[] = [
      ['screen and (max-width: 640px)', {
        seriesBarDistance: 5,
        axisX: {
          labelInterpolationFnc: function (value) {
            return value[0];
          }
        }
      }]
    ];

    new Chartist.Line('#chartActivity', data, options, responsiveOptions);

    var dataPreferences = {
      series: [
        [25, 30, 20, 25]
      ]
    };

    var optionsPreferences = {
      donut: true,
      donutWidth: 40,
      startAngle: 0,
      total: 100,
      showLabel: false,
      axisX: {
        showGrid: false
      }
    };

    new Chartist.Pie('#chartPreferences', dataPreferences, optionsPreferences);

    new Chartist.Pie('#chartPreferences', {
      labels: ['62%', '32%', '6%'],
      series: [62, 32, 6]
    });
  }

  AddBar(barName: string, barImageUrl: string) {
    console.log(barName + ',' + barImageUrl);
    const g = new Game(new Team('s', 'v'), new Team('s2', 'v2'));
    this.http.post<Game>(this.AddBarUrl + barName, g, this.httpOptions).subscribe((value) => {
      console.log(value);
    });
    // this.http.get(this.AddBarUrl + barName).subscribe(res => {
    //     console.log(res);
    //   },
    //   error1 => {
    //     console.log(error1);
    //   });
  }

  AddGame(gameDate: string, gameTeamA: string) {
    console.log(gameDate + ',' + gameTeamA);
    this.http.get(this.AddGameUrl).subscribe(res => {
        console.log(res);
      },
      error1 => {
        console.log(error1);
      });
  }
}

