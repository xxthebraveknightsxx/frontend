import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminComponent} from './admin/admin.component';
import {RouterModule, Routes} from '@angular/router';
import {GameSelectionComponent} from '../main-display/game-selection/game-selection.component';
import {SidebarModule} from '../shared/modules/sidebar/sidebar.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import {NavbarModule} from '../shared/modules/navbar/navbar.module';

const routes: Routes = [
  {
    path: '', component: AdminComponent,
    children: [
      {path: 'dashboard', component: DashboardComponent},
    ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SidebarModule,
    NavbarModule,
  ],
  declarations: [AdminComponent, DashboardComponent]
})
export class AdminModule {
}
