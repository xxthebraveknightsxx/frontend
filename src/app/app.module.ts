import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {GameSelectionService} from './shared/services/GameSelectionService';
import {GameListService} from './shared/services/GameListService';
import {MainDisplayModule} from './main-display/main-display.module';
import {BookBarModule} from './book-bar/book-bar.module';
import {AdminModule} from './admin/admin.module';
import {SharedModule} from './shared/shared.module';

const appRoutes: Routes = [
  {path: 'main-display', loadChildren: './main-display/main-display.module#MainDisplayModule'},
  {path: 'book', loadChildren: './book-bar/book-bar.module#BookBarModule'},
  {path: 'admin', loadChildren: './admin/admin.module#AdminModule'},
  {path: '', redirectTo: 'main-display', pathMatch: 'full'},

];


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
    ),
    SharedModule,
    // MainDisplayModule,
    // BookBarModule,
    // AdminModule
  ],
  providers: [GameSelectionService, GameListService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
