import {Component, OnInit} from '@angular/core';
import {GameSelectionService} from '../../shared/services/GameSelectionService';
import {GameListService} from '../../shared/services/GameListService';
import {Team} from '../../shared/models/Team';
import {Game} from '../../shared/models/Game';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.css']
})
export class DatepickerComponent implements OnInit {

  constructor(private gameListService: GameListService) {
  }

  ngOnInit() {
  }

  GetGamesForDate() {
    this.gameListService.load();
  }

}
