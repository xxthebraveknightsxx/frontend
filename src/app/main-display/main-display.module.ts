import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {GameListComponent} from './game-list/game-list.component';
import {GamePanelComponent} from './game-panel/game-panel.component';
import {DatepickerComponent} from './datepicker/datepicker.component';
import {GameSelectionComponent} from './game-selection/game-selection.component';
import {SharedModule} from '../shared/shared.module';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: '', component: GameSelectionComponent}
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    GameListComponent,
    GamePanelComponent,
    DatepickerComponent,
    GameSelectionComponent,
  ],
  exports: [
    GameListComponent,
    GamePanelComponent,
    DatepickerComponent,
    GameSelectionComponent,
  ]
})
export class MainDisplayModule { }
