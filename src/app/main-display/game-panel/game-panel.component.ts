import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {DEFAULT_GAME, Game} from '../../shared/models/Game';
import {GameSelectionService} from '../../shared/services/GameSelectionService';

@Component({
  selector: 'app-game-panel',
  templateUrl: './game-panel.component.html',
  styleUrls: ['./game-panel.component.css']
})
export class GamePanelComponent implements OnInit {
  DisplayedGame: Game;

  constructor(private gameSelectionService: GameSelectionService, private router: Router) {
    this.DisplayedGame = DEFAULT_GAME;
  }


  ngOnInit() {
    this.gameSelectionService.change.subscribe(game => {
      this.DisplayedGame = game;
    });
  }

  BookGame(teamName: string) {
    this.router.navigate(['book/' + teamName]);
  }


}
