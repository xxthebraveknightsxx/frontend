import {Component, OnInit} from '@angular/core';
import {Game} from '../../shared/models/Game';
import {GameSelectionService} from '../../shared/services/GameSelectionService';
import {GameListService} from '../../shared/services/GameListService';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit {
  GameList: Game[];

  constructor(private gameSelectionService: GameSelectionService, private gameListService: GameListService) {
  }

  ngOnInit() {
    this.gameListService.change.subscribe(
      gameList => this.GameList = gameList
    );
  }

  mouseEnter(game: Game) {
    console.log(game);
    this.gameSelectionService.select(game);
  }

  mouseLeave(game: Game) {
  }

}
