import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-team-circle',
  templateUrl: './team-circle.component.html',
  styleUrls: ['./team-circle.component.css']
})
export class TeamCircleComponent implements OnInit {
  @Input()TeamName: string;
  @Input()ImageUrl: string;
  constructor() {
  }

  ngOnInit() {
  }

}
