import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-bar-rectangle',
  templateUrl: './bar-rectangle.component.html',
  styleUrls: ['./bar-rectangle.component.css']
})
export class BarRectangleComponent implements OnInit {
  @Input() BarName: string;
  @Input() ImageUrl: string;

  constructor() {
  }

  ngOnInit() {
  }

}
