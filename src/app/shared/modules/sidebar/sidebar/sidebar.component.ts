import {Component, OnInit} from '@angular/core';

declare var $: any;

export interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  {path: 'dashboard', title: 'Dashboard', icon: 'ti-panel', class: ''},
  {path: 'user', title: 'User Profile', icon: 'ti-user', class: ''},
  {path: 'table', title: 'Table List', icon: 'ti-view-list-alt', class: ''},
  {path: 'maps', title: 'Maps', icon: 'ti-map', class: ''},
  {path: 'notifications', title: 'Notifications', icon: 'ti-bell', class: ''},
];

@Component({
  // moduleId: module.id,
  selector: 'app-sidebar',
  templateUrl: 'sidebar.component.html',
  styleUrls: [ 'sidebar.component.css']
})

export class SidebarComponent implements OnInit {
  public menuItems: any[];

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem.title);
  }

  isNotMobileMenu() {
    if (window.innerWidth > 991) {
      return false;
    }
    return true;
  }

}
