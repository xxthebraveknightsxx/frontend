import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BarRectangleComponent} from './components/bar-rectangle/bar-rectangle.component';
import {TeamCircleComponent} from './components/team-circle/team-circle.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    BarRectangleComponent,
    TeamCircleComponent,
  ],
  exports: [
    BarRectangleComponent,
    TeamCircleComponent,
  ]
})
export class SharedModule { }
