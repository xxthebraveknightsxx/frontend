export class Team {
  constructor(public Name: string, public ImageUrl: string) {
  }
}

export const DEFAULT_TEAM = new Team('Default', 'http://www.espn.com/i/teamlogos/soccer/500/default-team-logo-500.png?h=100&w=100');
