import {DEFAULT_TEAM, Team} from './Team';

export class Game {
  constructor(public TeamA: Team, public TeamB: Team) {
  }
}

export const DEFAULT_GAME = new Game(DEFAULT_TEAM, DEFAULT_TEAM);
