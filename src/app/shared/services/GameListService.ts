import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Game} from '../models/Game';

@Injectable()
export class GameListService {
  GameList: Game[];
  GameListUrl = 'assets/api/gamesList/gamesList.json';

  constructor(private http: HttpClient) {
  }

  @Output() change: EventEmitter<Game[]> = new EventEmitter();

  load() {
    this.http.get<Game[]>(this.GameListUrl).subscribe(data => {
        this.GameList = data;
        this.change.emit(this.GameList);
      },
      error1 => {
        console.log(error1);
      });
  }
}
