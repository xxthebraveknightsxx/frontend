import {EventEmitter, Injectable, Output} from '@angular/core';
import {Game} from '../models/Game';

@Injectable()
export class GameSelectionService {
  selectedGame: Game;

  @Output() change: EventEmitter<Game> = new EventEmitter();

  select(game: Game) {
    this.selectedGame = game;
    this.change.emit(this.selectedGame);
  }
}
