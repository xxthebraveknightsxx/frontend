import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Game} from '../../shared/models/Game';


@Component({
  selector: 'app-book-game',
  templateUrl: './book-game.component.html',
  styleUrls: ['./book-game.component.css']
})
export class BookGameComponent implements OnInit {
  BookGameUrl = 'assets/api/bookGame/bookGame.json';
  constructor(private route: ActivatedRoute, private http: HttpClient) {
    this.route.params.subscribe(params => console.log(params));
  }

  ngOnInit() {
  }

  BookGame() {
    this.http.get(this.BookGameUrl).subscribe(res => {
        console.log(res);
      },
      error1 => {
        console.log(error1);
      });
  }

}
