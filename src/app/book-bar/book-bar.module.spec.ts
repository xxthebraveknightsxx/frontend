import { BookBarModule } from './book-bar.module';

describe('BookBarModule', () => {
  let bookBarModule: BookBarModule;

  beforeEach(() => {
    bookBarModule = new BookBarModule();
  });

  it('should create an instance', () => {
    expect(bookBarModule).toBeTruthy();
  });
});
