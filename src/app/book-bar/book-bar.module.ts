import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BookGameComponent} from './book-game/book-game.component';
import {SharedModule} from '../shared/shared.module';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: ':teamName', component: BookGameComponent}
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    BookGameComponent
  ],
  exports: [
    BookGameComponent
  ]
})
export class BookBarModule {
}
